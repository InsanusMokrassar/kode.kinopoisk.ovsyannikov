package kode.kinopoisk.ovsyannikov.utils.JSONHandlers;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import kode.kinopoisk.ovsyannikov.R;
import kode.kinopoisk.ovsyannikov.utils.JSONHandlers.exceptions.HandlingException;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class SortHandlerTest {

    @Test
    public void mustCorrectSort() throws JSONException, HandlingException {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        List<JSONObject> from = new ArrayList<>();

        String[] path = appContext.getResources().getStringArray(R.array.miniPreviewRatingPath);

        for (int i = 10; i >= 0; i--) {
            from.add(new JSONObject("{\"" + path[0] + "\": \"" + Float.valueOf(i) + "\"}"));
        }

        List<JSONObject> sorted = new SortHandler().handler(appContext, from, new JSONObject("{\"sort\":\"asc\"}"));

        Collections.reverse(from);
        for (int i = 0; i < from.size(); i++) {
            assertEquals(from.get(i), sorted.get(i));
        }
    }

    @Test
    public void mustCorrectSortDesc() throws JSONException, HandlingException {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        List<JSONObject> from = new ArrayList<>();

        String[] path = appContext.getResources().getStringArray(R.array.miniPreviewRatingPath);

        for (int i = 0; i < 10; i++) {
            from.add(new JSONObject("{\"" + path[0] + "\": \"" + Float.valueOf(i) + "\"}"));
        }

        List<JSONObject> sorted = new SortHandler().handler(appContext, from, new JSONObject("{\"sort\":\"desc\"}"));

        Collections.reverse(from);
        for (int i = 0; i < from.size(); i++) {
            assertEquals(from.get(i), sorted.get(i));
        }
    }

}