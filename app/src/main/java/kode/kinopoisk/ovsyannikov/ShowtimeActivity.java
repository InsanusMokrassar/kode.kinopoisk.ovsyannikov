package kode.kinopoisk.ovsyannikov;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import kode.kinopoisk.ovsyannikov.adapters.MiniPreviewAdapter;
import kode.kinopoisk.ovsyannikov.fragments.ChooseFiltersFragment;
import kode.kinopoisk.ovsyannikov.fragments.ChooseSortMethodFragment;
import kode.kinopoisk.ovsyannikov.utils.JSONHandlers.FilterHandler;
import kode.kinopoisk.ovsyannikov.utils.JSONHandlers.SortHandler;
import kode.kinopoisk.ovsyannikov.utils.controllers.RequestsController;
import kode.kinopoisk.ovsyannikov.utils.controllers.SharedPreferencesController;
import kode.kinopoisk.ovsyannikov.utils.mailboxes.interfaces.MailBox;

public class ShowtimeActivity extends AppCompatActivity {

    protected MiniPreviewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_showtime);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    /**
     * Dispatch onResume() to fragments.  Note that for better inter-operation
     * with older versions of the platform, at the point of this call the
     * fragments attached to the activity are <em>not</em> resumed.  This means
     * that in some cases the previous state may still be saved, not allowing
     * fragment transactions that modify the state.  To correctly interact
     * with fragments in their proper state, you should instead override
     * {@link #onResumeFragments()}.
     */
    @Override
    protected void onResume() {
        super.onResume();

        Button locationBtn = (Button) findViewById(R.id.locationBtn);
        locationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent locationIntent = new Intent(getApplicationContext(), ChooseCityActivity.class);
                locationIntent.putExtra(getString(R.string.launchedFromOther), true);
                startActivity(locationIntent);
            }
        });
        locationBtn.setText(SharedPreferencesController.getCityName(this));

        findViewById(R.id.sortBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChooseSortMethodFragment chooseSortFragment = new ChooseSortMethodFragment();
                chooseSortFragment.setTargetMailBox(new MailBox<String>() {
                    @Override
                    public void receive(String object) {
                        try {
                            adapter.setHandleSettings(
                                    new JSONObject(
                                            "{\"" + SortHandler.SORT_FIELD + "\": " +
                                                    "\"" + object + "\"}"
                                    ));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                chooseSortFragment.show(getSupportFragmentManager(), "sort");
            }
        });

        findViewById(R.id.filterBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChooseFiltersFragment chooseFiltersFragment = new ChooseFiltersFragment();
                chooseFiltersFragment.setTargetMailBox(new MailBox<List<String>>() {
                    @Override
                    public void receive(List<String> object) {
                        JSONArray newChecked = new JSONArray(object);
                        try {
                            adapter.setHandleSettings(
                                    new JSONObject(
                                            "{\"" + FilterHandler.FILTER_FIELD + "\": " + newChecked + "}"
                                    ));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                chooseFiltersFragment.show(getSupportFragmentManager(), "filter");
            }
        });

        initMiniPreviewListView();
    }

    protected void initMiniPreviewListView() {
        GridView miniPreviewGridView = (GridView) findViewById(R.id.cinemaMiniPreviewGridView);
        miniPreviewGridView.setEmptyView(findViewById(R.id.loadMiniPreviewProgressBar));

        adapter = new MiniPreviewAdapter(this);
        miniPreviewGridView.setAdapter(adapter);
        RequestsController.refreshFilmsToday(
                this,
                adapter,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {}//TODO:: finish it
                }
        );

        miniPreviewGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long id) {
                SharedPreferencesController.setFilmId(getApplicationContext(), id);
                Intent fullPreviewIntent = new Intent(
                        getApplicationContext(),
                        FullPreviewCinemaActivity.class
                );
                startActivity(fullPreviewIntent);
            }
        });
    }
}
