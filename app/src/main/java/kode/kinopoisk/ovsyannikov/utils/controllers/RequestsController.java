package kode.kinopoisk.ovsyannikov.utils.controllers;


import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import kode.kinopoisk.ovsyannikov.R;
import kode.kinopoisk.ovsyannikov.utils.helpers.FilesSaver;
import kode.kinopoisk.ovsyannikov.utils.mailboxes.interfaces.MailBox;

public class RequestsController {

    public static final String LOCATION_PREFERENCES_NAME = "locationPreferences";

    protected static RequestQueue queue = null;

    public static void refreshRequestQueue(Context c) {
        if (queue != null) {
            queue.stop();
        }
        queue = Volley.newRequestQueue(c);
        queue.start();
    }

    public static void refreshCountries(
            Context c,
            final MailBox<List<JSONObject>> targetMailBox,
            Response.ErrorListener errorListener) {
        final String countriesResponseSetField = c.getString(R.string.countriesSetField);
        
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                String.format(c.getString(R.string.apiRoot), c.getString(R.string.getPostfixCountryTemplate)),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject responseJSON = new JSONObject(response);
                            JSONArray countries = responseJSON.getJSONArray(countriesResponseSetField);
                            List<JSONObject> countriesList = new ArrayList<>();
                            for (int i = 0; i < countries.length(); i++) {
                                countriesList.add(countries.getJSONObject(i));
                            }
                            targetMailBox.receive(countriesList);
                        } catch (JSONException e) {
                            e.printStackTrace();//TODO:: fix it
                        }
                    }
                },
                errorListener
        );
        queue.add(stringRequest);
    }

    public static void refreshCities(
            Context c, String countryId,
            final MailBox<List<JSONObject>> targetMailBox,
            Response.ErrorListener errorListener) {
        final String citiesResponseSetField = c.getString(R.string.citySetField);

        String targetUrlPostfix = String.format(c.getString(R.string.getPostfixCitiesTemplate), countryId);

        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                String.format(c.getString(R.string.apiRoot), targetUrlPostfix),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject responseJSON = new JSONObject(response);
                            JSONArray cities = responseJSON.getJSONArray(citiesResponseSetField);
                            List<JSONObject> citiesList = new ArrayList<>();
                            for (int i = 0; i < cities.length(); i++) {
                                citiesList.add(cities.getJSONObject(i));
                            }
                            targetMailBox.receive(citiesList);
                        } catch (JSONException e) {
                            e.printStackTrace();//TODO:: fix it
                        }
                    }
                },
                errorListener
        );
        queue.add(stringRequest);
    }

    public static void refreshFilmsToday(Context c, final MailBox<List<JSONObject>> targetMailBox, Response.ErrorListener errorListener) {
        final String cinemasResponseSetField = c.getString(R.string.miniFilmSetField);

        DateFormat format = new SimpleDateFormat(c.getString(R.string.dateFormat));

        String today = format.format(new Date());

        String targetUrlPostfix;
        try {
            targetUrlPostfix = String.format(
                    c.getString(R.string.getPostfixCinemasTodayTemplate),
                    today,
                    SharedPreferencesController.getCityId(c)
                            .toString()
            );
        } catch (NullPointerException e) {
            Log.d("RequestController", "Can't get city id");
            return;
        }

        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                String.format(c.getString(R.string.apiRoot), targetUrlPostfix),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject responseJSON = new JSONObject(response);
                            JSONArray cities = responseJSON.getJSONArray(cinemasResponseSetField);
                            List<JSONObject> citiesList = new ArrayList<>();
                            for (int i = 0; i < cities.length(); i++) {
                                citiesList.add(cities.getJSONObject(i));
                            }
                            targetMailBox.receive(citiesList);
                        } catch (JSONException e) {
                            e.printStackTrace();//TODO:: fix it
                        }
                    }
                },
                errorListener
        );
        queue.add(stringRequest);
    }

    public static void refreshCinemas(
            Context c,
            Long cityId,
            final MailBox<JSONArray> targetMailBox,
            Response.ErrorListener errorListener) {
        String targetUrlPostfix = String.format(
                c.getString(R.string.getPostfixCinemasTemplate),
                cityId.toString()
        );
        final String cinemasResponseSetField = c.getString(R.string.cinemasSetField);

        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                String.format(c.getString(R.string.apiRoot), targetUrlPostfix),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject responseJSON = new JSONObject(response);
                            JSONArray cities = responseJSON.getJSONArray(cinemasResponseSetField);
                            targetMailBox.receive(cities);
                        } catch (JSONException e) {
                            e.printStackTrace();//TODO:: fix it
                        }
                    }
                },
                errorListener
        );
        queue.add(stringRequest);
    }

    public static void downloadAndReturnImage(
            final Context c,
            Long id,
            final MailBox<Uri> target) {

        final String filePath = String.format(
                c.getString(R.string.imagesFilePathTemplate),
                id.toString()
        );
        Uri prePath = FilesSaver.getFile(filePath, c);
        if (prePath != null){
            target.receive(prePath);
            return;
        }
        String url = String.format(
                c.getString(R.string.imagesTemplate),
                id.toString()
        );

        ImageRequest imageRequest = new ImageRequest(
                url,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                        target.receive(FilesSaver.saveImage(c, response, filePath));
                    }
                },
                c.getResources().getInteger(R.integer.downloadImagesSize),
                c.getResources().getInteger(R.integer.downloadImagesSize),
                ImageView.ScaleType.CENTER,
                null,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        );
        queue.add(imageRequest);
    }

    public static void downloadImage(
            final Context c,
            Long id) {

        final String filePath = String.format(
                c.getString(R.string.imagesFilePathTemplate),
                id.toString()
        );
        if (FilesSaver.getFile(filePath, c) != null){
            return;
        }
        String url = String.format(
                c.getString(R.string.imagesTemplate),
                id.toString()
        );

        ImageRequest imageRequest = new ImageRequest(
                url,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                    }
                },
                c.getResources().getInteger(R.integer.downloadImagesSize),
                c.getResources().getInteger(R.integer.downloadImagesSize),
                ImageView.ScaleType.CENTER,
                null,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        );
        queue.add(imageRequest);
    }

    public static void refreshFilm(
            Context c,
            Long filmId,
            final MailBox<JSONObject> targetMailBox,
            Response.ErrorListener errorListener) {

        String targetUrlPostfix = String.format(
                c.getString(R.string.getPostfixFilmTemplate),
                filmId.toString()
        );

        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                String.format(c.getString(R.string.apiRoot), targetUrlPostfix),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject responseJSON = new JSONObject(response);
                            targetMailBox.receive(responseJSON);
                        } catch (JSONException e) {
                            e.printStackTrace();//TODO:: fix it
                        }
                    }
                },
                errorListener
        );
        queue.add(stringRequest);
    }

    public static void refreshSeances(
            final Context c,
            final MailBox<List<JSONObject>> targetMailBox,
            Response.ErrorListener errorListener) {

        DateFormat format = new SimpleDateFormat(c.getString(R.string.dateFormat));

        String today = format.format(new Date());

        String targetUrlPostfix = String.format(
                c.getString(R.string.getPostfixSeancesTemplate),
                SharedPreferencesController.getFilmId(c).toString(),
                SharedPreferencesController.getCityId(c).toString(),
                today
        );
        final String cinemasResponseSetField = c.getString(R.string.seancesCinemasSetField);

        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                String.format(c.getString(R.string.apiRoot), targetUrlPostfix),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject responseJSON = new JSONObject(response);

                            JSONArray cinemas = responseJSON.getJSONArray(cinemasResponseSetField);
                            List<JSONObject> seancesList = new ArrayList<>();
                            for (int i = 0; i < cinemas.length(); i++) {
                                seancesList.add(cinemas.getJSONObject(i));
                            }

                            SharedPreferencesController.setSeances(c, seancesList);

                            targetMailBox.receive(seancesList);
                        } catch (JSONException e) {
                            e.printStackTrace();//TODO:: fix it
                        }
                    }
                },
                errorListener
        );
        queue.add(stringRequest);
    }

    public static void refreshGenres(final Context c) {
        if (SharedPreferencesController.getGenres(c) == null) {
            final String genresSetField = c.getString(R.string.genresSetField);

            StringRequest stringRequest = new StringRequest(
                    Request.Method.GET,
                    String.format(c.getString(R.string.apiRoot), c.getString(R.string.getPostfixGenres)),
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject responseJSON = new JSONObject(response);

                                JSONArray genres = responseJSON.getJSONArray(genresSetField);
                                List<JSONObject> genresList = new ArrayList<>();
                                for (int i = 0; i < genres.length(); i++) {
                                    genresList.add(genres.getJSONObject(i));
                                }

                                SharedPreferencesController.setGenres(c, genresList);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }
            );
            queue.add(stringRequest);
        }
    }
}
