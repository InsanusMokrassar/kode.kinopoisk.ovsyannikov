package kode.kinopoisk.ovsyannikov.utils.mailboxes.interfaces;

public interface MailBox<T> {

    void receive(T object);

}
