package kode.kinopoisk.ovsyannikov.utils.JSONHandlers;


import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import kode.kinopoisk.ovsyannikov.R;
import kode.kinopoisk.ovsyannikov.utils.JSONHandlers.exceptions.HandlingException;
import kode.kinopoisk.ovsyannikov.utils.JSONHandlers.interfaces.JSONHandler;

public class SortHandler implements JSONHandler {

    public static final String SORT_FROM_GRAND_TO_LOWER = "desc";
    public static final String SORT_FROM_LOWER_TO_GRAND = "asc";
    public static final String SORT_FIELD = "sort";

    /**
     * Sort films by rating
     * @param c
     * @param from
     * @param settings
     * Await:<pre>
     *     {
     *         "sort" : "asc" | "desc"
     *     }
     * </pre>
     * @return
     * @throws HandlingException
     */
    @Override
    public List<JSONObject> handler(Context c, List<JSONObject> from, JSONObject settings) throws HandlingException {


        try {
            if (settings.getString(SORT_FIELD).equals(SORT_FROM_LOWER_TO_GRAND)) {
                return sortAsc(from, c.getResources().getStringArray(R.array.miniPreviewRatingPath));
            } else {
                return sortDesc(from, c.getResources().getStringArray(R.array.miniPreviewRatingPath));
            }
        } catch (JSONException e) {
            throw new HandlingException("Can't sort:(", e);
        }
    }

    protected static List<JSONObject> sortAsc(List<JSONObject> from, String... ratingPathField) throws JSONException {//choose bad algorythm cause time comes to an end

        List<JSONObject> result = new LinkedList<>();

        List<JSONObject> temporary = new ArrayList<>();
        temporary.addAll(from);

        Integer startSize = temporary.size();

        for (int i = 0; i < startSize; i++) {

            JSONObject currentToAdd = null;
            Float lastRating = null;
            for (int j = 0; j < temporary.size(); j++) {
                String rating = (String) getValueByPath(temporary.get(j), ratingPathField);
                rating = rating.split(" ")[0];
                Float ratingFloat = Float.valueOf(rating);
                try {
                    if (lastRating > ratingFloat) {
                        lastRating = ratingFloat;
                        currentToAdd = temporary.get(j);
                    }
                } catch (NullPointerException e) {
                    lastRating = ratingFloat;
                    currentToAdd = temporary.get(j);
                }
            }
            temporary.remove(currentToAdd);
            result.add(currentToAdd);
        }

        return result;
    }

    protected static List<JSONObject> sortDesc(List<JSONObject> from, String... ratingPathField) throws JSONException {//choose bad algorythm cause time comes to an end

        List<JSONObject> result = new LinkedList<>();

        List<JSONObject> temporary = new ArrayList<>();
        temporary.addAll(from);

        Integer startSize = temporary.size();

        for (int i = 0; i < startSize; i++) {

            JSONObject currentToAdd = null;
            Float lastRating = null;
            for (int j = 0; j < temporary.size(); j++) {
                String rating = (String) getValueByPath(temporary.get(j), ratingPathField);
                rating = rating.split(" ")[0];
                Float ratingFloat = Float.valueOf(rating);
                try {
                    if (lastRating < ratingFloat) {
                        lastRating = ratingFloat;
                        currentToAdd = temporary.get(j);
                    }
                } catch (NullPointerException e) {
                    lastRating = ratingFloat;
                    currentToAdd = temporary.get(j);
                }
            }
            temporary.remove(currentToAdd);
            result.add(currentToAdd);
        }

        return result;
    }

    protected static Object getValueByPath(JSONObject from, String... path) throws JSONException {//so slow and bad for read, i know
        Object value = from;
        JSONObject current = from;
        JSONArray currentArray = null;
        for (String pathCurrent : path) {
            try {
                current = current.getJSONObject(pathCurrent);
            } catch (JSONException e) {
                try {
                    currentArray = current.getJSONArray(pathCurrent);
                    current = null;
                } catch (JSONException e1) {
                    try {
                        value = current.get(pathCurrent);
                    } catch (JSONException e2) {
                        value = currentArray.get(Integer.valueOf(pathCurrent));
                    }
                }
            } catch (NullPointerException e) {
                try {
                    current = currentArray.getJSONObject(Integer.valueOf(pathCurrent));
                } catch (NullPointerException e1) {
                    throw new JSONException("Can't get value");
                } catch (JSONException e1) {
                    try {
                        currentArray = currentArray.getJSONArray(Integer.valueOf(pathCurrent));
                    } catch (JSONException e2) {
                            value = currentArray.get(Integer.valueOf(pathCurrent));
                    }
                }
            }
        }
        return value;
    }
}
