package kode.kinopoisk.ovsyannikov.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class JSONHelper {

    /**
     * Concatenate all JSON objects(last will be concatenate last and him fields will write always)
     * @param objects Input objects
     * @return New generated JSONObject
     */
    public static JSONObject concat(JSONObject... objects){
        JSONObject res = new JSONObject();
        for (JSONObject current : objects){
            Iterator<String> iterator = current.keys();
            while (iterator.hasNext()){
                String key = iterator.next();
                try {
                    res.put(key, current.get(key));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return res;
    }

    public static Object[] toArray(JSONArray jsonArray) {
        Object[] array = new Object[jsonArray.length()];
        for (int i = 0; i < array.length; i++) {
            try {
                array[i] = jsonArray.get(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return array;
    }

    public static <T> List<T> asList(JSONArray jsonArray) {
        List<T> objects = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                objects.add((T)jsonArray.get(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return objects;
    }
}
