package kode.kinopoisk.ovsyannikov.utils.JSONHandlers.exceptions;

/**
 * Created by aleksey on 23.10.16.
 */

public class HandlingException extends Exception {

    public HandlingException(String message, Throwable cause) {
        super(message, cause);
    }
}
