package kode.kinopoisk.ovsyannikov.utils.JSONHandlers;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import kode.kinopoisk.ovsyannikov.R;
import kode.kinopoisk.ovsyannikov.utils.JSONHandlers.exceptions.HandlingException;
import kode.kinopoisk.ovsyannikov.utils.JSONHandlers.interfaces.JSONHandler;
import kode.kinopoisk.ovsyannikov.utils.JSONHelper;

public class FilterHandler implements JSONHandler{

    public static final String FILTER_FIELD = "filter";


    //in future we can use other structure for expanded use filter
    @Override
    public List<JSONObject> handler(Context c, List<JSONObject> from, JSONObject settings) throws HandlingException {
        try {
            JSONArray checked = settings.getJSONArray(FILTER_FIELD);
            if (checked.length() == 0) {
                return from;
            }
            List<String> checkedList = JSONHelper.asList(checked);
            List<JSONObject> result = new ArrayList<>();
            for (JSONObject fromObject : from) {
                String genresOfObject = fromObject.getString(c.getString(R.string.filmGenreField));
                for (String genre : checkedList) {
                    if (genresOfObject.contains(genre)) {
                        result.add(fromObject);
                        break;
                    }
                }
            }
            return result;
        } catch (JSONException e) {
            throw new HandlingException("Can't filter objects", e);
        }
    }


}
