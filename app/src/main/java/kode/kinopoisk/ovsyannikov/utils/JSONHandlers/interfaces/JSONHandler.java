package kode.kinopoisk.ovsyannikov.utils.JSONHandlers.interfaces;

import android.content.Context;

import org.json.JSONObject;

import java.util.List;

import kode.kinopoisk.ovsyannikov.utils.JSONHandlers.exceptions.HandlingException;

public interface JSONHandler {

    List<JSONObject> handler(Context c, List<JSONObject> from, JSONObject settings) throws HandlingException;
}
