package kode.kinopoisk.ovsyannikov.utils.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import kode.kinopoisk.ovsyannikov.utils.mailboxes.interfaces.MailBox;

public class FilesSaver {

    public static Uri saveImage(Context c, Bitmap bitmapFile, String name){
        try {
            String folder = getAbsolutePath(c);
            File file = new File(folder, name);
            if (!file.exists()) {
                OutputStream fOut = new FileOutputStream(file);
                bitmapFile.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                fOut.flush();
                fOut.close();
            }
            return Uri.parse(file.toString());
        } catch (Exception e) {//TODO:: // FIXME: 22.10.16 write handlers for exceptions
            e.printStackTrace();
        }
        return null;
    }

    public static String getAbsolutePath(Context c) {
        String folder = "";
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            try {
                folder = c.getExternalCacheDir().getAbsolutePath();
            }
            catch (Exception e){
                folder = c.getCacheDir().getAbsolutePath();
            }
        }
        return folder;
    }

    public static Uri getFile(String relationPath, Context c) throws NullPointerException{
        String folder = getAbsolutePath(c);
        File file = new File(folder + "/" + relationPath);
        if (!file.exists()){
            return null;
        }
        return Uri.parse(file.toString());
    }
}
