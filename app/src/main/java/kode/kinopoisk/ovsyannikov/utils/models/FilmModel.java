package kode.kinopoisk.ovsyannikov.utils.models;


import org.json.JSONException;
import org.json.JSONObject;

public class FilmModel {
    protected String ageRating;
    protected String worldPremier;
    protected String rating;
    protected String country;
    protected String genre;
    protected String length;
    protected String description;

    protected JSONObject from;

    public FilmModel(JSONObject from) throws JSONException {
        this.from = from;

        try {
            worldPremier = from
                    .getJSONObject("rentData")
                    .getString("premiereWorld");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            rating = from
                    .getJSONObject("ratingData")
                    .getString("rating");
        } catch (JSONException e) {//if not found
            try {
                rating = from
                        .getJSONObject("ratingData")
                        .getString("ratingIMDb");
            } catch (JSONException e1) {
                rating = from
                        .getJSONObject("ratingData")
                        .getString("ratingAwait");
            }
        }
        try {
            ageRating = from.getString("ratingAgeLimits");
        } catch (JSONException e) {
            ageRating = null;
        }
        try {
            length = from.getString("filmLength");
        } catch (JSONException e) {
            length = "?";
        }
        country = from.getString("country");
        genre = from.getString("genre");
        description = from.getString("description");
    }

    public String getAgeRating() {
        return ageRating;
    }

    public String getWorldPremier() {
        return worldPremier;
    }

    public String getRating() {
        return rating;
    }

    public String getCountry() {
        return country;
    }

    public String getGenre() {
        return genre;
    }

    public String getLength() {
        return length;
    }

    public String getDescription() {
        return description;
    }

    public JSONObject getFrom() {
        return from;
    }
}
