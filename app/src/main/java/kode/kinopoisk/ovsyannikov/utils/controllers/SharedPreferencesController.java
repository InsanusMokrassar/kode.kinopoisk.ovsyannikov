package kode.kinopoisk.ovsyannikov.utils.controllers;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kode.kinopoisk.ovsyannikov.R;
import kode.kinopoisk.ovsyannikov.utils.helpers.SharedPreferencesHelper;

public class SharedPreferencesController {
    
    public static Long getCityId(Context c) {
        try {
            return Long.valueOf(
                    SharedPreferencesHelper.getData(
                            c,
                            c.getString(R.string.sharedPreferencesName),
                            c.getString(R.string.cityIdField)
                    ).get(
                            c.getString(R.string.cityIdField)
                    ).toString()
            );
        } catch (NullPointerException e) {//TODO:: // FIXME: 22.10.16
            return null;
        }
    }

    public static String getCityName(Context c) {
        try {
            return SharedPreferencesHelper.getData(
                    c,
                    c.getString(R.string.sharedPreferencesName),
                    c.getString(R.string.cityNameField)
            ).get(
                    c.getString(R.string.cityNameField)
            ).toString();
        } catch (NullPointerException e) {//TODO:: // FIXME: 22.10.16
            return null;
        }
    }
    
    public static void setCity(Context c, JSONObject object) {
        Map<String, String> data = new HashMap<>();
        try {
            data.put(c.getString(R.string.cityIdField), object.getLong(c.getString(R.string.cityIdField)) + "");
            data.put(c.getString(R.string.cityNameField), object.getString(c.getString(R.string.cityNameField)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        SharedPreferencesHelper.putData(c, c.getString(R.string.sharedPreferencesName), data);
    }

    public static void setCinemas(Context c, JSONArray cinemas) {
        Map<String, String> data = new HashMap<>();
        data.put(c.getString(R.string.cinemasSetField), cinemas.toString());
        SharedPreferencesHelper.putData(c, c.getString(R.string.sharedPreferencesName), data);
    }

    public static List<JSONObject> getCinemas(Context c) {
        try {
            JSONArray cinemas = new JSONArray(SharedPreferencesHelper.getData(
                    c,
                    c.getString(R.string.sharedPreferencesName),
                    c.getString(R.string.cinemasSetField)
            ).get(c.getString(R.string.cinemasSetField)).toString());

            List<JSONObject> cinemasList = new ArrayList<>();
            for (int i = 0; i < cinemas.length(); i++) {
                cinemasList.add(cinemas.getJSONObject(i));
            }
            return cinemasList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static void setGenres(Context c, List<JSONObject> genres) {
        String separator = c.getString(R.string.jsonObjectsSeparator);

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < genres.size(); i++) {
            stringBuilder.append(genres.get(i));
            if (i < genres.size() - 1) {
                stringBuilder.append(separator);
            }
        }

        Map<String, String> resultMap = new HashMap<>();

        resultMap.put(c.getString(R.string.genresVariable), stringBuilder.toString());

        SharedPreferencesHelper.putData(c, c.getString(R.string.sharedPreferencesName), resultMap);
    }

    public static List<JSONObject> getGenres(Context c) {
        String separator = c.getString(R.string.jsonObjectsSeparatorForSplitting);
        try {
            String concatenatedString = (String) SharedPreferencesHelper.getData(
                    c,
                    c.getString(R.string.sharedPreferencesName),
                    c.getString(R.string.genresVariable)
            ).get(c.getString(R.string.genresVariable));

            List<String> stringPresent = Arrays.asList(
                    concatenatedString.split(separator)
            );

            List<JSONObject> genresObjects = new ArrayList<>();
            for (String genreString : stringPresent) {
                genresObjects.add(new JSONObject(genreString));
            }

            return genresObjects;
        } catch (NullPointerException e) {
            return null;
        } catch (JSONException e) {
            return null;
        }
    }

    public static void setFilmId(Context c, Long filmId) {

        Map<String, String> resultMap = new HashMap<>();

        resultMap.put(c.getString(R.string.filmIdField), filmId.toString());

        SharedPreferencesHelper.putData(c, c.getString(R.string.sharedPreferencesName), resultMap);

    }

    public static Long getFilmId(Context c) {
        try {
            return Long.valueOf(
                    SharedPreferencesHelper.getData(
                            c,
                            c.getString(R.string.sharedPreferencesName),
                            c.getString(R.string.filmIdField)
                    ).get(
                            c.getString(R.string.filmIdField)
                    ).toString()
            );
        } catch (NullPointerException e) {//TODO:: // FIXME: 22.10.16
            return null;
        }
    }


    public static void setCheckedGenres(Context c, List<String> checkedGenres) {
        String separator = c.getString(R.string.jsonObjectsSeparator);

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < checkedGenres.size(); i++) {
            stringBuilder.append(checkedGenres.get(i));
            if (i < checkedGenres.size() - 1) {
                stringBuilder.append(separator);
            }
        }

        Map<String, String> resultMap = new HashMap<>();

        resultMap.put(c.getString(R.string.checkedGenresVariable), stringBuilder.toString());

        SharedPreferencesHelper.putData(c, c.getString(R.string.sharedPreferencesName), resultMap);

    }

    public static List<String> getCheckedGenres(Context c) {
        String separator = c.getString(R.string.jsonObjectsSeparatorForSplitting);
        try {
            String concatenatedString = (String) SharedPreferencesHelper.getData(
                    c,
                    c.getString(R.string.sharedPreferencesName),
                    c.getString(R.string.checkedGenresVariable)
            ).get(c.getString(R.string.checkedGenresVariable));

            List<String> checkedGenres = Arrays.asList(
                    concatenatedString.split(separator)
            );

            return checkedGenres;
        } catch (NullPointerException e){
            setCheckedGenres(c, new ArrayList<String>());
            return new ArrayList<>();
        }
    }


    public static void setSeances(Context c, List<JSONObject> seances) {
        String separator = c.getString(R.string.jsonObjectsSeparator);

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < seances.size(); i++) {
            stringBuilder.append(seances.get(i));
            if (i < seances.size() - 1) {
                stringBuilder.append(separator);
            }
        }

        Map<String, String> resultMap = new HashMap<>();

        resultMap.put(c.getString(R.string.seancesVariable), stringBuilder.toString());

        SharedPreferencesHelper.putData(c, c.getString(R.string.sharedPreferencesName), resultMap);
    }

    public static List<JSONObject> getSeances(Context c) {
        String separator = c.getString(R.string.jsonObjectsSeparatorForSplitting);
        try {
            String concatenatedString = (String) SharedPreferencesHelper.getData(
                    c,
                    c.getString(R.string.sharedPreferencesName),
                    c.getString(R.string.seancesVariable)
            ).get(c.getString(R.string.seancesVariable));

            List<String> stringPresent = Arrays.asList(
                    concatenatedString.split(separator)
            );

            List<JSONObject> seancesObjects = new ArrayList<>();
            for (String seanceString : stringPresent) {
                seancesObjects.add(new JSONObject(seanceString));
            }

            return seancesObjects;
        } catch (NullPointerException e) {
            return null;
        } catch (JSONException e) {
            return null;
        }
    }
}
