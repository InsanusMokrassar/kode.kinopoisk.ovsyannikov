package kode.kinopoisk.ovsyannikov.utils.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;

public class SharedPreferencesHelper {

    public static Map<String, Object> getData(Context c, String preferencesName, String... fields){
        Map<String, Object> data = new HashMap<>();

        SharedPreferences preferences = c.getSharedPreferences(preferencesName, Context.MODE_PRIVATE);

        Map<String, ?> preferencesData = preferences.getAll();

        for (String field : fields) {
            if (preferencesData.containsKey(field)) {
                data.put(field, preferencesData.get(field));
            }
        }

        return data;
    }

    public static void putData(Context c, String preferencesName, Map<String, String> objects){
        SharedPreferences preferences = c.getSharedPreferences(preferencesName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        for (String key : objects.keySet()) {
            editor.putString(key, objects.get(key));
        }

        editor.apply();

    }

}
