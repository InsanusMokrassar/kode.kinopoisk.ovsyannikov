package kode.kinopoisk.ovsyannikov.utils.mailboxes.interfaces;

import java.util.ArrayList;
import java.util.List;

public class TransmissionMailBox<T> implements MailBox<T> {

    protected List<MailBox<T>> mailBoxes = new ArrayList<>();

    public void addMailBox(MailBox<T> mailBox) {
        if (!mailBoxes.contains(mailBox)) {
            mailBoxes.add(mailBox);
        }
    }

    public void removeMailBox(MailBox<T> mailBox) {
        mailBoxes.remove(mailBox);
    }

    @Override
    public void receive(T object) {
        for (MailBox<T> mailbox : mailBoxes) {
            mailbox.receive(object);
        }
    }
}
