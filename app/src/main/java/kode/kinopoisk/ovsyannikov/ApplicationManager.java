package kode.kinopoisk.ovsyannikov;

import android.app.Application;

import kode.kinopoisk.ovsyannikov.utils.controllers.RequestsController;


public class ApplicationManager extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        RequestsController.refreshRequestQueue(this);
        RequestsController.refreshGenres(this);
    }
}
