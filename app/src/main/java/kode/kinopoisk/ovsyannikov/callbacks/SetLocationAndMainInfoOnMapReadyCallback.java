package kode.kinopoisk.ovsyannikov.callbacks;

import android.content.Context;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import kode.kinopoisk.ovsyannikov.R;

/**
 * Created by aleksey on 24.10.16.
 */

public class SetLocationAndMainInfoOnMapReadyCallback implements OnMapReadyCallback {

    protected List<JSONObject> seances;
    protected String cinemaTitleField;
    protected String cinemaAddressField;
    protected String timeField;
    protected String seancesField;
    protected String latitudeField;
    protected String longitudeField;

    public SetLocationAndMainInfoOnMapReadyCallback(Context c, List<JSONObject> seances) {
        this.seances = seances;
        cinemaAddressField = c.getString(R.string.seancesCinemaAddressField);
        cinemaTitleField = c.getString(R.string.seancesCinemaNameField);
        timeField = c.getString(R.string.seancesTimeField);
        latitudeField = c.getString(R.string.seancesLatitudeField);
        longitudeField = c.getString(R.string.seancesLongitudeField);
        seancesField = c.getString(R.string.seancesSetField);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        List<Marker> markers = new ArrayList<>();

        for (JSONObject seance : seances) {
            try {
                Double lat = seance.getDouble(latitudeField);
                Double lon = seance.getDouble(longitudeField);

                StringBuilder timesForSnippet = new StringBuilder();
                JSONArray times = seance.getJSONArray(seancesField);
                for (int i = 0; i < times.length(); i++) {
                    timesForSnippet.append(times.getJSONObject(i).getString(timeField)).append("; ");
                }


                markers.add(googleMap.addMarker(
                        new MarkerOptions()
                        .position(new LatLng(
                            lat, lon
                        ))
                        .title(
                                seance.getString(cinemaTitleField) + ", " +
                                seance.getString(cinemaAddressField)
                        )
                        .snippet(timesForSnippet.toString())
                ));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker marker : markers) {
            builder.include(marker.getPosition());
        }
        LatLngBounds bounds = builder.build();

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 50);

        googleMap.moveCamera(cu);
    }
}
