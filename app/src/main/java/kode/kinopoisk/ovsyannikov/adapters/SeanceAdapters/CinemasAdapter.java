package kode.kinopoisk.ovsyannikov.adapters.SeanceAdapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import kode.kinopoisk.ovsyannikov.R;
import kode.kinopoisk.ovsyannikov.adapters.AbstractJSONAdapter;
import kode.kinopoisk.ovsyannikov.utils.JSONHelper;
import kode.kinopoisk.ovsyannikov.utils.mailboxes.interfaces.MailBox;

public class CinemasAdapter extends AbstractJSONAdapter implements MailBox<List<JSONObject>> {

    protected String seancesField;
    protected String titleField;
    protected String addressField;

    public CinemasAdapter(Context c) {
        super(c);
        init(c);
    }

    protected void init(Context c) {
        seancesField = c.getString(R.string.seancesSetField);
        titleField = c.getString(R.string.seancesCinemaNameField);
        idField = c.getString(R.string.seancesCinemaIdField);
        addressField = c.getString(R.string.seancesCinemaAddressField);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View resultView = inflate(view, viewGroup, R.layout.cinema_seance_item);

        JSONObject current = getItem(i);

        try {
            ((TextView)resultView.findViewById(R.id.cinemaTitle))
                    .setText(
                            current.getString(titleField)
                    );
            ((TextView)resultView.findViewById(R.id.cinemaAddress))
                    .setText(
                            current.getString(addressField)
                    );

            GridView timesView = (GridView) resultView.findViewById(R.id.seancesTimesGridPane);

            timesView.setAdapter(
                    new TimesAdapter(
                            JSONHelper.<JSONObject>asList(current.getJSONArray(seancesField)),
                            inflater.getContext()
                    )
            );
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return resultView;
    }

    @Override
    public void receive(List<JSONObject> object) {
        objects = object;
        notifyDataSetChanged();
    }

    public List<JSONObject> getObjects() {
        List<JSONObject> forReturn = new ArrayList<>();

        for (JSONObject object : objects) {
            forReturn.add(JSONHelper.concat(object));
        }

        return forReturn;
    }
}
