package kode.kinopoisk.ovsyannikov.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import kode.kinopoisk.ovsyannikov.R;
import kode.kinopoisk.ovsyannikov.utils.mailboxes.interfaces.MailBox;

public class CitySimpleAdapter extends AbstractJSONAdapter implements MailBox<List<JSONObject>> {

    protected String textField;

    public CitySimpleAdapter(Context c) {
        super(c);
        idField = c.getString(R.string.cityIdField);
        textField = c.getString(R.string.cityNameField);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View resultView = inflate(view, viewGroup, android.R.layout.simple_list_item_1);

        TextView countryPresentTextView = (TextView) resultView.findViewById(android.R.id.text1);
        try {
            countryPresentTextView.setText(objects.get(i).getString(textField));
        } catch (JSONException e) {
            countryPresentTextView.setText("Error");//TODO:: fix it
        }

        return resultView;
    }

    @Override
    public void receive(List<JSONObject> newObject) {
        objects = newObject;
        notifyDataSetChanged();
    }
}
