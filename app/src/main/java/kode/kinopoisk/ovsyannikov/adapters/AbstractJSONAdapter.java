package kode.kinopoisk.ovsyannikov.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractJSONAdapter extends BaseAdapter {

    protected List<JSONObject> objects = new ArrayList<>();
    protected String idField;

    protected LayoutInflater inflater;

    public AbstractJSONAdapter(List<JSONObject> objects, Context c) {
        this.objects = objects;
        inflater = LayoutInflater.from(c);
    }

    public AbstractJSONAdapter(Context c) {
        inflater = LayoutInflater.from(c);
    }

    public AbstractJSONAdapter(List<JSONObject> objects, String idField, Context c) {
        this.objects = objects;
        this.idField = idField;
        inflater = LayoutInflater.from(c);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public JSONObject getItem(int i) {
        return objects.get(i);
    }

    @Override
    public long getItemId(int i) {
        try {
            return Long.valueOf(objects.get(i).getString(idField));
        } catch (JSONException e) {
            return 0;
        }
    }

    protected View inflate(View view, ViewGroup viewGroup, int resource) {
        return view == null ? inflater.inflate(resource, viewGroup, false) : view;
    }
}
