package kode.kinopoisk.ovsyannikov.adapters;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import kode.kinopoisk.ovsyannikov.R;
import kode.kinopoisk.ovsyannikov.utils.JSONHandlers.FilterHandler;
import kode.kinopoisk.ovsyannikov.utils.JSONHandlers.SortHandler;
import kode.kinopoisk.ovsyannikov.utils.JSONHandlers.exceptions.HandlingException;
import kode.kinopoisk.ovsyannikov.utils.JSONHandlers.interfaces.JSONHandler;
import kode.kinopoisk.ovsyannikov.utils.JSONHelper;
import kode.kinopoisk.ovsyannikov.utils.controllers.RequestsController;
import kode.kinopoisk.ovsyannikov.utils.controllers.SharedPreferencesController;
import kode.kinopoisk.ovsyannikov.utils.mailboxes.interfaces.MailBox;

/**
 * Created by aleksey on 21.10.16.
 */

public class MiniPreviewAdapter extends AbstractJSONAdapter implements MailBox<List<JSONObject>>{

    protected List<JSONObject> srcList;

    protected String titleField;
    protected String ratingField;
    protected String genreField;
    protected String premierDateField;

    protected JSONObject handleSettings;
    protected List<JSONHandler> handlers = new ArrayList<>();

    public MiniPreviewAdapter(Context c) {
        super(c);

        idField = c.getString(R.string.miniFilmIdField);
        titleField = c.getString(R.string.filmTitleField);
        ratingField = c.getString(R.string.miniFilmRatingField);
        premierDateField = c.getString(R.string.filmPremierDateField);
        genreField = c.getString(R.string.miniFilmGenreField);
        try {
            handleSettings = new JSONObject(c.getString(R.string.miniPreviewHandlePreSettings));
            handleSettings.put(FilterHandler.FILTER_FIELD, new JSONArray(SharedPreferencesController.getCheckedGenres(c)));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        handlers.add(new SortHandler());
        handlers.add(new FilterHandler());
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        final View resultView = inflate(view, viewGroup, R.layout.mini_preview_cinema_item);

        JSONObject current = objects.get(i);

        try {
            ((TextView)resultView.findViewById(R.id.fullPreviewTitleTextView))
                    .setText(current.getString(titleField));

        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(), e.getMessage());
        }
        try {
            ((TextView)resultView.findViewById(R.id.ratingTextView))
                    .setText(current.getString(ratingField));
        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(), e.getMessage());
        }
        try {
            ((TextView)resultView.findViewById(R.id.premierDateTextView))
                    .setText(current.getString(premierDateField));

        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(), e.getMessage());
        }
        try {
            ((TextView)resultView.findViewById(R.id.genreTextView))
                    .setText(current.getString(genreField));

        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(), e.getMessage());
        }
        RequestsController.downloadAndReturnImage(
                inflater.getContext(),
                getItemId(i),
                new MailBox<Uri>() {
                    @Override
                    public void receive(Uri object) {
                        ((ImageView) resultView.findViewById(R.id.miniPreviewImageView))
                                .setImageURI(object);
                    }
                }
        );

        return resultView;
    }

    public void setHandleSettings(JSONObject handleSettings) {
        this.handleSettings = JSONHelper.concat(this.handleSettings, handleSettings);
        refreshList();
    }

    @Override
    public void receive(List<JSONObject> object) {
        srcList = object;
        refreshList();
    }

    protected void refreshList() {
        objects = srcList;
        for (JSONHandler handler : handlers) {
            try {
                objects = handler.handler(inflater.getContext(), objects, handleSettings);
            } catch (HandlingException e) {
                e.printStackTrace();
            }
        }
        notifyDataSetChanged();
    }
}
