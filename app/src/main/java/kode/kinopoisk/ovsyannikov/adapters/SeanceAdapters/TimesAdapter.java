package kode.kinopoisk.ovsyannikov.adapters.SeanceAdapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import kode.kinopoisk.ovsyannikov.R;
import kode.kinopoisk.ovsyannikov.adapters.AbstractJSONAdapter;

public class TimesAdapter extends AbstractJSONAdapter {

    protected String timeField;

    public TimesAdapter(List<JSONObject> objects, Context c) {
        super(objects, c);
        timeField = c.getString(R.string.seancesTimeField);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View resultView = inflate(view, viewGroup, android.R.layout.simple_list_item_1);

        JSONObject current = getItem(i);

        try {
            ((TextView) resultView.findViewById(android.R.id.text1))
                    .setText(
                            current.getString(timeField)
                    );
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return resultView;
    }
}
