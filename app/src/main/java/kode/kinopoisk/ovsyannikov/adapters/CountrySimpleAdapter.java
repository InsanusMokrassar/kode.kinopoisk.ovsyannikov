package kode.kinopoisk.ovsyannikov.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import kode.kinopoisk.ovsyannikov.R;
import kode.kinopoisk.ovsyannikov.utils.mailboxes.interfaces.MailBox;

public class CountrySimpleAdapter extends AbstractJSONAdapter implements MailBox<List<JSONObject>> {

    protected String textField;

    public CountrySimpleAdapter(List<JSONObject> objects, Context c) {
        super(objects, c);
        idField = c.getString(R.string.countryIdField);
        textField = c.getString(R.string.countryNameField);
    }

    public CountrySimpleAdapter(Context c) {
        super(c);
        idField = c.getString(R.string.countryIdField);
        textField = c.getString(R.string.countryNameField);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View resultView = inflate(view, viewGroup, android.R.layout.simple_dropdown_item_1line);

        TextView countryPresentTextView = (TextView) resultView.findViewById(android.R.id.text1);
        try {
            countryPresentTextView.setText(objects.get(i).getString(textField));
        } catch (JSONException e) {
            countryPresentTextView.setText("Error");//TODO:: fix it
        }

        return resultView;
    }

    @Override
    public void receive(List<JSONObject> newObject) {
        objects = newObject;
        notifyDataSetChanged();
    }
}
