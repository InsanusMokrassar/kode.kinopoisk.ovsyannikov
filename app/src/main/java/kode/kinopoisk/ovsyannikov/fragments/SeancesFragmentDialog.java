package kode.kinopoisk.ovsyannikov.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.List;

import kode.kinopoisk.ovsyannikov.R;
import kode.kinopoisk.ovsyannikov.adapters.SeanceAdapters.CinemasAdapter;
import kode.kinopoisk.ovsyannikov.utils.controllers.RequestsController;
import kode.kinopoisk.ovsyannikov.utils.mailboxes.interfaces.MailBox;
import kode.kinopoisk.ovsyannikov.utils.mailboxes.interfaces.TransmissionMailBox;

public class SeancesFragmentDialog extends DialogFragment {

    protected CinemasAdapter adapter;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        adapter = new CinemasAdapter(getActivity());

        View usingView = LayoutInflater.from(getActivity()).inflate(R.layout.seances_dialog_layout, null);

        builder.setView(usingView);

        ListView seancesListView = (ListView) usingView.findViewById(R.id.seancesListView);
        seancesListView.setAdapter(adapter);

        View seancesListViewProgressBar = usingView.findViewById(R.id.seancesListViewProgressBar);
        seancesListView.setEmptyView(seancesListViewProgressBar);

        TransmissionMailBox<List<JSONObject>> transmissionMailBox = new TransmissionMailBox<>();
        transmissionMailBox.addMailBox(adapter);

        RequestsController.refreshSeances(
                getActivity(),
                transmissionMailBox,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {}
                }
        );

        final View mapButton = usingView.findViewById(R.id.cinemasMapButton);
        mapButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CinemasSeancesGoogleMapDialog dialog = new CinemasSeancesGoogleMapDialog();
                        dialog.show(getFragmentManager(), "maps");
                    }
                }
        );

        transmissionMailBox.addMailBox(new MailBox<List<JSONObject>>() {
            @Override
            public void receive(List<JSONObject> object) {
                mapButton.setVisibility(View.VISIBLE);
            }
        });

        return builder.create();
    }
}
