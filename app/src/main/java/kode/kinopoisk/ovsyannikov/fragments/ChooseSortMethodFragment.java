package kode.kinopoisk.ovsyannikov.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Toast;

import kode.kinopoisk.ovsyannikov.R;
import kode.kinopoisk.ovsyannikov.utils.mailboxes.interfaces.MailBox;

public class ChooseSortMethodFragment extends DialogFragment {

    protected MailBox<String> resultSort;

    public void setTargetMailBox(MailBox<String> resultSort) {
        this.resultSort = resultSort;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.chooseSortTitle)
                .setItems(
                        R.array.ratingSortVariants,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                resultSort.receive(
                                        getResources().getStringArray(R.array.ratingSortResults)[i]
                                );
                            }
                        }
                )
        ;
        return builder.create();
    }
}
