package kode.kinopoisk.ovsyannikov.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import kode.kinopoisk.ovsyannikov.R;
import kode.kinopoisk.ovsyannikov.utils.controllers.SharedPreferencesController;
import kode.kinopoisk.ovsyannikov.utils.mailboxes.interfaces.MailBox;

public class ChooseFiltersFragment extends DialogFragment {

    protected MailBox<List<String>> resultFilter;

    protected String[] genres;
    protected List<String> checkedGenres;

    protected boolean[] checked;

    public void setTargetMailBox(MailBox<List<String>> resultFilter) {
        this.resultFilter = resultFilter;
    }

    protected void refreshChecked() {
        checkedGenres = SharedPreferencesController.getCheckedGenres(getActivity());
        checked = new boolean[genres.length];
        Arrays.fill(checked, false);
        for (int i = 0; i < genres.length; i++) {
                if (checkedGenres.contains(genres[i])) {
                    checked[i] = true;
                }
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        List<JSONObject> genresList = SharedPreferencesController.getGenres(getContext());

        try {
            String genreNameField = getString(R.string.genresNameField);

            genres = new String[genresList.size()];
            for (int i = 0; i < genresList.size(); i++) {
                JSONObject genre = genresList.get(i);
                genres[i] = genre.getString(genreNameField);
            }
        } catch (NullPointerException | JSONException e) {
            Toast.makeText(getContext(), "Can't get genres", Toast.LENGTH_SHORT).show();
        }
        refreshChecked();

        builder.setTitle(R.string.chooseFilterTitle)
                .setMultiChoiceItems(
                        genres,
                        checked,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                                checked[i] = b;
                            }
                        }
                )
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int number) {
                        List<String> checkedGenres = new ArrayList<>();
                        for (int i = 0; i < checked.length; i++) {
                            if (checked[i]) {
                                checkedGenres.add(genres[i]);
                            }
                        }
                        SharedPreferencesController.setCheckedGenres(getActivity(), checkedGenres);
                        resultFilter.receive(checkedGenres);
                    }
                })
                .setNeutralButton(R.string.clear, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        checkedGenres = new ArrayList<>();
                        SharedPreferencesController.setCheckedGenres(getActivity(), checkedGenres);
                        resultFilter.receive(checkedGenres);
                    }
                })
        ;
        return builder.create();
    }
}
