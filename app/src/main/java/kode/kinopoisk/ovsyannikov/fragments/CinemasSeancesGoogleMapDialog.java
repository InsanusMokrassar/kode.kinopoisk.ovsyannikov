package kode.kinopoisk.ovsyannikov.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.google.android.gms.maps.MapFragment;

import kode.kinopoisk.ovsyannikov.R;
import kode.kinopoisk.ovsyannikov.callbacks.SetLocationAndMainInfoOnMapReadyCallback;
import kode.kinopoisk.ovsyannikov.utils.controllers.SharedPreferencesController;

public class CinemasSeancesGoogleMapDialog extends DialogFragment {

    protected MapFragment mMapFragment;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View resultView = LayoutInflater
                .from(getActivity())
                .inflate(R.layout.cinemas_seances_google_maps_dialog, null);

        builder.setView(resultView);

        mMapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.cinemasMap);

        mMapFragment.getMapAsync(
                new SetLocationAndMainInfoOnMapReadyCallback(
                        getActivity(),
                        SharedPreferencesController.getSeances(getActivity())
                )
        );

        return builder.create();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.remove(mMapFragment);
            transaction.commit();
        } catch (IllegalStateException e) {//for example, if phone was rotated
            e.printStackTrace();
        }
    }
}
