package kode.kinopoisk.ovsyannikov;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Spinner;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;

import kode.kinopoisk.ovsyannikov.adapters.CitySimpleAdapter;
import kode.kinopoisk.ovsyannikov.adapters.CountrySimpleAdapter;
import kode.kinopoisk.ovsyannikov.utils.controllers.RequestsController;
import kode.kinopoisk.ovsyannikov.utils.controllers.SharedPreferencesController;
import kode.kinopoisk.ovsyannikov.utils.mailboxes.interfaces.MailBox;

public class ChooseCityActivity extends AppCompatActivity {

    protected Boolean fromOther;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_city);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fromOther = getIntent().getBooleanExtra(getString(R.string.launchedFromOther), false);
        if (!fromOther) {
            if (SharedPreferencesController.getCityId(this) != null) {
                callNext();
            }
        }

        initCountrySpinner();

    }

    public void initCountrySpinner() {
        CountrySimpleAdapter countrySimpleAdapter = new CountrySimpleAdapter(this);

        RequestsController.refreshCountries(
                this,
                countrySimpleAdapter,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {}//TODO:: finish it
                }
        );

        final Spinner countrySpinner = (Spinner)findViewById(R.id.countrySpinner);
        countrySpinner.setAdapter(countrySimpleAdapter);

        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long id) {
                initCityListView(id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void initCityListView(Long countryId) {
        findViewById(R.id.citiesContainer).setVisibility(View.VISIBLE);
        GridView citiesListView = (GridView) findViewById(R.id.citiesGridView);

        citiesListView.setEmptyView(findViewById(R.id.citiesProgressBar));

        final CitySimpleAdapter adapter = new CitySimpleAdapter(this);
        citiesListView.setAdapter(adapter);

        RequestsController.refreshCities(
                this,
                countryId.toString(),
                adapter,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {}//TODO:: finish it
                }
        );

        citiesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long id) {
                SharedPreferencesController.setCity(getApplicationContext(), adapter.getItem(i));
                RequestsController.refreshCinemas(
                        getApplicationContext(),
                        id,
                        new MailBox<JSONArray>() {
                            @Override
                            public void receive(JSONArray object) {
                                SharedPreferencesController.setCinemas(
                                        getApplicationContext(),
                                        object
                                );
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {}
                        }
                );
                callNext();
            }
        });
    }

    protected void callNext() {
        if (fromOther) {
            finish();
            return;
        }
        Intent nextActivity = new Intent(this, ShowtimeActivity.class);
        startActivity(nextActivity);
    }

}
