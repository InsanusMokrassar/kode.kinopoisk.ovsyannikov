package kode.kinopoisk.ovsyannikov;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import kode.kinopoisk.ovsyannikov.fragments.SeancesFragmentDialog;
import kode.kinopoisk.ovsyannikov.utils.controllers.RequestsController;
import kode.kinopoisk.ovsyannikov.utils.controllers.SharedPreferencesController;
import kode.kinopoisk.ovsyannikov.utils.mailboxes.interfaces.MailBox;
import kode.kinopoisk.ovsyannikov.utils.models.FilmModel;

import static android.view.View.GONE;

public class FullPreviewCinemaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_preview_cinema);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Long filmId = SharedPreferencesController.getFilmId(this);

        RequestsController.refreshFilm(//start hell hardcode:(
                this,
                filmId,
                new MailBox<JSONObject>() {
                    @Override
                    public void receive(JSONObject object) {
                        RequestsController.downloadAndReturnImage(
                                getApplicationContext(),
                                filmId,
                                new MailBox<Uri>() {
                                    @Override
                                    public void receive(Uri object) {
                                        ((ImageView) findViewById(R.id.fullPreviewImageView))
                                                .setImageURI(object);
                                    }
                                }
                        );


                        try {
                            ((TextView) findViewById(R.id.fullPreviewTitleTextView))
                                    .setText(
                                            object.getString(getString(R.string.filmTitleField))
                                    );

                            FilmModel model = new FilmModel(object);

                            if (model.getAgeRating() != null) {
                                ((TextView) findViewById(R.id.fullPreviewAgeRatingTextView)).setText(
                                        String.format(
                                                getString(R.string.fullPreviewAgeRatingTemplate),
                                                model.getAgeRating())
                                );
                            } else {
                                findViewById(R.id.fullPreviewAgeRatingTextView).setVisibility(GONE);
                            }
                            ((TextView) findViewById(R.id.fullPreviewCountryTextView))
                                    .setText(
                                            String.format(
                                                    getString(R.string.fullPreviewCountryTemplate),
                                                    model.getCountry()
                                            )
                                    );
                            ((TextView) findViewById(R.id.fullPreviewGenreTextView))
                                    .setText(
                                            String.format(
                                                    getString(R.string.fullPreviewGenreTemplate),
                                                    model.getGenre()
                                            )
                                    );
                            ((TextView) findViewById(R.id.fullPreviewRatingTextView))
                                    .setText(
                                            String.format(
                                                    getString(R.string.fullPreviewRatingTemplate),
                                                    model.getRating()
                                            )
                                    );
                            ((TextView) findViewById(R.id.fullPreviewLengthTextView))
                                    .setText(
                                            String.format(
                                                    getString(R.string.fullPreviewLengthTemplate),
                                                    model.getLength()
                                            )
                                    );
                            if (model.getWorldPremier() != null) {
                                ((TextView) findViewById(R.id.fullPreviewWorldPremierTextView))
                                        .setText(
                                                String.format(
                                                        getString(R.string.fullPreviewWorldPremierTemplate),
                                                        model.getWorldPremier()
                                                )
                                        );
                            } else {
                                findViewById(R.id.fullPreviewWorldPremierTextView).setVisibility(GONE);
                            }
                            ((TextView) findViewById(R.id.fullPreviewDescriptionTextView))
                                    .setText(
                                            String.format(
                                                    getString(R.string.fullPreviewDescriptionTemplate),
                                                    model.getDescription()
                                            )
                                    );

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {}
                }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.full_preview_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.seancesMenuItem) {
            SeancesFragmentDialog seancesFragmentDialog = new SeancesFragmentDialog();
            seancesFragmentDialog.show(getFragmentManager(), "seances");
        }

        return super.onOptionsItemSelected(item);
    }
}
